# PyYJKS


#### 介绍
pyYJKS是盈建科结构设计软件基于Python语言提供的二次开发接口，用户可以使用pyYJKS对盈建科建筑结构计算软件进行二次开发，实现模型、项目、构件参数化批量创建、批量修改、自定义荷载施加、前处理、自动计算、提取设计结果、模型迭代优化、参数化出图、智能设计、个性化定制等；pyYJKS结合Python强大且丰富的第三方扩展库可以为结构设计赋予无限发展空间。


#### 模块与API
pyYJKS是建立在盈建科结构设计软件基础之上的，其模块需要在YJK-A运行时才能够工作， pyYJKS提供一整套机制来扩展和展示YJK-A的功能。pyYJKS程序包含了读写YJK-A中DB级别的pyYJKSModel模块、YJK-A的所有界面操作和定制YJK-A UI的pyYJKSUI模块、对模型进行前处理的pyYJKSPre模块、对构件添加特殊构件属性定义的pyYJKSParam模块、执行和自定义YJK-A命令的pyYJKSCommand模块以及整体数据输出和构件设计结果输出的设计结果pyYJKSDesign模块。

建议用户在开始使用pyYJKS之前，请先熟悉盈建科结构设计软件基本功能；掌握API（Application Programming Interface，应用程序接口）的类及成员函数；如果对盈建科结构设计软件或pyYJKS不熟悉请前往盈建科产品展示中心网站（https:www.yjk.cn/jgrj/）了解更多信息。


#### 安装教程
 **_
[PYTHON下载](https://www.python.org/downloads/windows/)
注意：为了避免因python版本导致的奔溃无法运行的问题，请安装3.8.4版本python。

_** 
1.  安装python3.8.4
![输入图片说明](image.png)
2.  将替换文件中的内容复制并替换到YJK安装目录下（可参考视频）
3.  将python安装文件下的python38.dll复制替换到YJK安装目录下（可参考视频）
4.  安装Numpy库

#### 使用说明

1.  安装包内包含操作流程视频与API文档，使用前请阅读使用说明。
2.  在YJK中运行命令yjks_pyload加载py文件（可重复执行）
![输入图片说明](Screenshot_20220615164307.png)
3.  导入py文件并生成模型
![输入图片说明](ff665108-ec88-11ec-9599-d85ed331302a.png)
4.  运行结果及错误信息  
![输入图片说明](111image.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
